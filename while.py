from random import randint

num=1
while(num<101):
    print(num)
    num+=1
num=0
while(num<101):
    print(num)
    num+=2
num=0
while(num<101):
    print(num)
    num+=5
num=100
while(num>0):
    print(num)
    num-=1
num=100
while(num>=0):
    print(num)
    num-=2
num=100
while(num>=0):
    print(num)
    num-=5
num=25
while(num<=75):
    print(num)
    num+=5
num=75
while(num>=25):
    print(num)
    num-=5

start = randint(0,100)
end = randint(0,100)
print("Start: ",start)
print("End: ",end)
if(start>end):
    while(start>=end):
        print(start)
        start-=1
elif(end>start):
    while(start<=end):
        print(start)
        start+=1
else:
    print(start)