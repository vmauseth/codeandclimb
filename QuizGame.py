points=0
q=input("What year did 9/11 happen?")
if(q=="2001"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What year did World War 2 end?")
if(q=="1944" or q=="1945"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is the average life span of a dog?")
if(int(q)>=10 and int(q)<=13):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is the most played videogame of 2018?")
if(q=="Fortnite"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("Who is the actor of Aaron Burr in the original cast of Hamilton?")
if("Leslie Odom" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is the largest fresh water lake in the world?")
if(q=="Lake Superior"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is the world's longest river?")
if("Amazon" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("Who directed the first ever Star Wars?")
if(q=="George Lucas"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("Which chess piece can only move diagonally?")
if("Bishop" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What are the three primary colors?")
if(("Blue" in q) and ("Red" in q) and ("Yellow" in q)):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is sushi wrapped in?")
if(q=="Seaweed"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What is the name of Han Solo's assistant wookie?")
if(q=="Chewbacca"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("What color was Yoda's lightsaber in Attack of the Clone Wars?")
if(q=="Green"):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("Who painted the Starry Night?")
if("Van Gogh" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("When did Alexander Haminton and Aaron Burr duel?")
if("July 11" or "1804" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")
q=input("Which president had the shortest term in office?")
if("Henry" or "William" or "Harrison" in q):
    points+=1
    print("Correct! Good job!")
else:
    print("Incorrect")