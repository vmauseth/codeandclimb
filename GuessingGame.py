turn=0
from random import randint
num=randint(0,100)
top = 100
bottom = 0
while(True):
    guess=int(input("Guess a number between "+str(bottom)+" and "+str(top)+": "))
    turn+=1
    if(num<guess):
        top=guess
        print("Guess Lower")
    elif(num>guess):
        bottom = guess
        print("Guess Higher")
    else:
        print("You guessed it in "+str(turn)+" turns! Good Job!")
        break